package busfor.ua;

import org.openqa.selenium.WebDriver;

import java.net.URL;

public class ProjectConfiguration {

        private static WebDriver webDriver;

        private static URL url;

        public static WebDriver getWebDriver() {
                return webDriver;
        }

        public static URL getUrl() {
                return url;
        }

        public static void setWebDriver(WebDriver webDriver) {
                ProjectConfiguration.webDriver = webDriver;
        }

        public static void setUrl(URL url) {
                ProjectConfiguration.url = url;
        }

}
