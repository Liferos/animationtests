package informationLogger;


import java.io.*;
import java.util.Calendar;

public class Logger {

        private PrintWriter writer = null;
        private String sessionName = null;

        public Logger(String fileName) throws Exception{
                File file = new File(fileName);
                if(!file.exists() && !file.isFile()) {
                        file.createNewFile();
                        writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
                        writer.println(getTimeString()+"Log file has been created");
                } else {
                        writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
                }
        }

        public void finalize() {
                if(writer != null) {
                        writer.flush();
                        writer.close();
                }
        }

        private String getTimeString() {
                return "["+Calendar.getInstance().get(Calendar.YEAR)+"."+
                        (Integer.parseInt(String.valueOf(Calendar.getInstance().get(Calendar.MONTH)/10)) >= 1 ?
                                Calendar.getInstance().get(Calendar.MONTH) :
                                (String)("0"+Calendar.getInstance().get(Calendar.MONTH)))+"."+
                        (Integer.parseInt(String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH)/10)) >= 1 ?
                                Calendar.getInstance().get(Calendar.DAY_OF_MONTH) :
                                (String)("0"+Calendar.getInstance().get(Calendar.DAY_OF_MONTH)))+" "+
                        (Integer.parseInt(String.valueOf(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)/10)) >= 1 ?
                                Calendar.getInstance().get(Calendar.HOUR_OF_DAY) :
                                (String)("0"+Calendar.getInstance().get(Calendar.HOUR_OF_DAY)))+":"+
                        (Integer.parseInt(String.valueOf(Calendar.getInstance().get(Calendar.MINUTE)/10)) >= 1 ?
                                Calendar.getInstance().get(Calendar.MINUTE) :
                                (String)("0"+Calendar.getInstance().get(Calendar.MINUTE)))+"]";
        }

        public void pushMessage(String msg) {
                writer.println(getTimeString()+msg);
                System.out.println(getTimeString()+msg);
        }

        public void pushAssertion(String msg) {
                pushMessage(msg);
        }

        public void openSession(String name) {
                sessionName = name;
                writer.println(" ");
                System.out.println(" ");
                writer.println(" ");
                System.out.println(" ");
                writer.println(getTimeString()+"Session: "+name);
                System.out.println(getTimeString()+"Session: "+name);
                writer.println(" ");
                System.out.println(" ");
        }

        public void closeSession() {
                writer.println(" ");
                System.out.println(" ");
                writer.println(getTimeString()+"Close session: "+sessionName);
                System.out.println(getTimeString()+"Close session: "+sessionName);
                sessionName = null;
        }

}
