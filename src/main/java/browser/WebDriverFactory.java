package browser;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverFactory {

        public WebDriver getWebDriver(DriverType driverType) {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setPlatform(Platform.WINDOWS);
                capabilities.setJavascriptEnabled(true);
                try {
                        switch (driverType) {
                                case CHROME: {
                                        return new ChromeDriver();
                                }
                                case FIREFOX: {
                                        return new FirefoxDriver();
                                }
                                default: {
                                        throw new IllegalArgumentException();
                                }
                        }
                } catch(Exception e) {
                        e.printStackTrace();
                        return null;
                }
        }

}
