package page;

import browser.DriverType;
import browser.WebDriverFactory;
import org.openqa.selenium.*;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Page {

        private int y = 0;

        public WebDriver webDriver = null;

        public Page openPage(String url, DriverType driverType) {
                try {
                        webDriver = new WebDriverFactory().getWebDriver(driverType);
                        webDriver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);
                        webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
                        webDriver.get(url);
                        System.out.println("The page \""+webDriver.getTitle().trim()+"\" was open!");
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return this;
        }

        public void closePage() throws InterruptedException {
                Thread.sleep(2000);
                y = 0;
                try{
                        if(webDriver == null) {
                                throw new NullPointerException();
                        } else {
                                webDriver.quit();
                                System.out.println("Page was closed!");
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }

        public void waiting(int seconds) {
                try {
                        if(webDriver == null) {
                                new NullPointerException();
                        }
                        webDriver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }

        public WebElement waitingForElementToBeClickable(int seconds, By by) {
                WebElement dynamicElement = null;
                try {
                        if(webDriver == null) {
                                new NullPointerException();
                        }
                        dynamicElement = new WebDriverWait(webDriver, 20)
                                .until(ExpectedConditions.elementToBeClickable(by));
                } catch (Exception e) {
                        e.printStackTrace();
                }
                scrollToElement(dynamicElement);
                return dynamicElement;
        }

        public WebElement waitingForElementToBeLocated(int seconds, By by) {
                WebElement dynamicElement = null;
                try {
                        if(webDriver == null) {
                                new NullPointerException();
                        }
                        dynamicElement = new WebDriverWait(webDriver, 20)
                                .until(ExpectedConditions.presenceOfElementLocated(by));
                } catch (Exception e) {
                        e.printStackTrace();
                }
                scrollToElement(dynamicElement);
                return dynamicElement;
        }

        public WebElement findElement(By by) {
                WebElement webElement = null;
                try {
                        if(webDriver == null) {
                                new NullPointerException();
                        }
                        webElement = webDriver.findElement(by);
                } catch(Exception e) {
                        e.printStackTrace();
                }
                scrollToElement(webElement);
                return webElement;
        }

        public void scrollToElement(WebElement element) {
                try {
                        ((JavascriptExecutor)webDriver).executeScript("window.scrollBy(0,"+((element.getLocation().getY()-50)-y)+")", "");
                        Thread.sleep(500);
                } catch(Exception ex) {
                        ex.printStackTrace();
                }
                y = element.getLocation().getY()-50;
                if(y<=0) {
                        y = 0;
                        return;
                }
        }

        public WebDriver getPage() {
                try {
                        if(webDriver == null) {
                                throw new NullPointerException();
                        }
                        return webDriver;
                } catch(Exception e) {
                        e.printStackTrace();
                }
                return null;
        }

        public void setPage(WebDriver page) {
                try {
                        if(page == null) {
                                throw new NullPointerException();
                        }
                        this.webDriver = page;
                } catch(Exception e) {
                        e.printStackTrace();
                }
        }

        public byte[] getObjectScreenshotAsByte(WebElement element) throws IOException {
                BufferedImage mImg = ImageIO.read(((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE));
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                ImageIO.write(mImg.getSubimage(element.getLocation().getX(),
                        element.getLocation().getY(),
                        element.getSize().getWidth(),
                        element.getSize().getHeight()),
                        "png",
                        byteArrayOutputStream);
                byteArrayOutputStream.flush();
                byte[] result = byteArrayOutputStream.toByteArray();
                return result;
        }

        public void pressButtonsFromString(String str) throws AWTException, InterruptedException {
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(new StringSelection(str), new ClipboardOwner() {
                        public void lostOwnership(Clipboard clipboard, Transferable contents) {

                        }
                });
                Robot robot = new Robot();
                robot.keyPress(KeyEvent.VK_CONTROL);
                robot.keyPress(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_V);
                robot.keyRelease(KeyEvent.VK_CONTROL);
        }

        public void pressButton(int key) throws Exception {
                Robot robot = new Robot();
                robot.keyPress(key);
                robot.keyRelease(key);
        }

}
