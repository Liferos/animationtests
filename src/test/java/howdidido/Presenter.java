package howdidido;


import browser.DriverType;
import informationLogger.Logger;
import org.im4java.core.CompareCmd;
import org.im4java.core.IMOperation;
import org.im4java.process.ProcessStarter;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import page.Page;

import java.io.*;
import java.util.Properties;
import java.awt.event.KeyEvent;
import java.util.List;

public class Presenter {
        private static Page page;
        private static Logger logger;
        private static Properties property;
        private static String pathToPropertyFile = "properties.txt";

        @BeforeClass
        public static void setUp() throws Exception {
                try {
                        FileInputStream fis = new FileInputStream(pathToPropertyFile);
                        property = new Properties();
                        property.load(fis);
                        fis.close();
                } catch (IOException ex) {
                        ex.printStackTrace();
                }
                page = new Page();
                logger = new Logger(property.getProperty("path.testingLogFile"));
        }

        @Test //P_HDID_001 E_HDID_001
        public void validLoginCredential() throws InterruptedException {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("validLoginCredential");
                logger.pushMessage("Open "+page.getPage().getTitle());


                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                Assert.assertTrue(page.getPage().getTitle().trim().equalsIgnoreCase("My Presentations | HowDidIDo"));
                logger.pushAssertion("Check successful user log in");

                page.closePage();
                logger.closeSession();
        }

        @Test //P_HDID_002 E_HDID_002
        public void invalidLoginCredential() throws InterruptedException {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("invalidLoginCredential");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.invalid"));
                logger.pushMessage("Input invalid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.invalid"));
                logger.pushMessage("Input invalid user's password");

                signInButton.click();
                logger.pushMessage("Click sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@id='LoginForm_password_em_']"));
                Assert.assertTrue(element.getText().trim().equalsIgnoreCase("Incorrect email or password."));
                logger.pushAssertion("Check presence of Error message");

                page.closePage();
                logger.closeSession();

        }

        @Test //P_HDID_003 E_HDID_003
        public void changeSettings() throws InterruptedException, IOException {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("changeSettings");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@class='dropdown-toggle user-menu']"));
                element.click();

                logger.pushMessage("Open dropdown user menu");

                element = page.findElement(By.xpath("//a[@href='/main/logout']"));
                Assert.assertTrue(element.isDisplayed());
                element = page.findElement(By.xpath("//a[@href='/profile']"));
                Assert.assertTrue(element.isDisplayed());

                logger.pushAssertion("Check that user menu being opened");

                element.click();
                logger.pushMessage("Click on user Settings button");

                element = page.findElement(By.xpath("//input[@name='file']"));
                element.sendKeys(property.getProperty("path.newImageAvatar"));
                logger.pushMessage("Pick new avatar picture");

                element = page.findElement(By.xpath("//input[@class='btn btn-save-profile']"));
                element.click();
                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10, By.xpath("//img[@alt='Merchant Logo']"));

                OutputStream os = new FileOutputStream("tmp.jpg");
                os.write(page.getObjectScreenshotAsByte(element));
                os.close();

                CompareCmd compare = new CompareCmd();
                IMOperation imo = new IMOperation();
                ProcessStarter.setGlobalSearchPath(property.getProperty("path.imgMagick"));
                imo.metric("ae");
                imo.addImage();
                imo.addImage();
                imo.addImage();

                File difference = new File("difference.jpg");
                difference.createNewFile();

                Boolean flagAssertion;
                try {
                        compare.run(imo, "tmp.jpg", "avatar.jpg", "difference.jpg");
                        flagAssertion = false;
                } catch(Exception ex) {
                        flagAssertion = true;
                }

                Assert.assertTrue(flagAssertion);
                logger.pushAssertion("Check that new avatar not similar with previous");

                element = page.findElement(By.xpath("//input[@name='file']"));
                logger.pushMessage("Download old avatar picture");
                element.sendKeys(property.getProperty("path.oldImageAvatar"));

                element = page.findElement(By.xpath("//input[@class='btn btn-save-profile']"));
                element.click();
                logger.pushMessage("Submit changes");

                difference.delete();
                File file = new File("tmp.jpg");
                file.delete();

                logger.closeSession();
                page.closePage();
        }

        @Test //P_HDID_004
        public void createVideoPresentation() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("createVideo");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();

                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                element.sendKeys("testVideo");
                logger.pushMessage("Input title of presentation");

                element = page.findElement(By.xpath("//button[@id='btn_create_presentation']"));
                element.click();
                logger.pushMessage("Click \"next\"");


                for(int i = 45; i < 51; i++) {
                        element = page.waitingForElementToBeClickable(10,
                                                                      By.xpath("//input[@name='questions["+i+"]']"));
                        element.sendKeys("yes");
                }
                logger.pushMessage("Input answers on the questions");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='lbl panel-title']"));
                element.click();

                logger.pushMessage("Click on \"upload a video file\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='dz-default dz-message']"));

                Action act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload testVideo");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[contains(text(), " +
                                                                        "'Video successfully uploaded.')]"));
                page.findElement(By.xpath("//div[@class='growl growl-notice growl-medium']/div[1]")).click();

                element = page.findElement(By.xpath("//button[@id='submit-form']"));
                element.click();

                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10,
                        By.xpath("//div[@id='modals-presentation-submit-success'][@aria-hidden='false']" +
                                                "/div/div/div[4]/button"));
                element.click();

                logger.pushMessage("Apply changes");


                element = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//button[@class='close delete-presentation']"));
                element.click();

                logger.pushMessage("Delete last presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='btn btn-danger']"));
                element.click();

                logger.pushMessage("Apply delete");

                logger.closeSession();
                page.closePage();
        }

        @Test //P_HDID_005
        public void createVideoWithAttachment() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("createVideoWithAttachment");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();
                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                element.sendKeys("testVideo");

                logger.pushMessage("Write in title of presentation");
                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@id='btn_create_presentation']"));
                element.click();
                logger.pushMessage("Click \"next\"");

                for(int i = 45; i < 51; i++) {
                        element = page.waitingForElementToBeClickable(10, By.xpath("//input[@name='questions["+i+"]']"));
                        element.sendKeys("yes");
                }
                logger.pushMessage("Write in answers on the questions");

                element = page.findElement(By.xpath("//span[@class='lbl panel-title']"));
                element.click();
                logger.pushMessage("Click on \"upload a video file\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='dz-default dz-message']"));


                Action act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload testVideo");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[contains(text(), " +
                        "'Video successfully uploaded.')]"));
                page.findElement(By.xpath("//div[@class='growl growl-notice growl-medium']/div[1]")).click();

                element = page.getPage().findElements(By.xpath("//div[@class='dz-default dz-message']")).get(1);
                page.scrollToElement(element);
                act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload video to attachment");

                element = page.waitingForElementToBeLocated(10,
                        By.xpath("//div[@id='presentation-files']/div[2]/div/div[3]/div/div[@class='dz-success-mark']"));

                element = page.findElement(By.xpath("//button[@id='submit-form']"));
                element.click();

                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10,
                        By.xpath("//div[@id='modals-presentation-submit-success'][@aria-hidden='false']" +
                                "/div/div/div[4]/button"));
                element.click();

                logger.pushMessage("Apply changes");

                element = page.waitingForElementToBeClickable(10,
                                        By.xpath("//button[@class='close delete-presentation']"));
                element.click();
                logger.pushMessage("Delete last presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='btn btn-danger']"));
                element.click();

                logger.pushMessage("Apply delete");

                logger.closeSession();
                page.closePage();
        }

        @Test //P_HDID_006
        public void deletePresentation() {
                logger.openSession("deletePresentation");
                logger.pushMessage("All actions were realized in other tests");
                logger.closeSession();
        }

        @Test //P_HDID_007
        public void createWithoutFullUploadingOfVideo() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);

                logger.openSession("createVideoWithoutFullUploadingOfVideo");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");


                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();

                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                element.sendKeys("testVideo");

                logger.pushMessage("Input title of presentation");

                element = page.findElement(By.xpath("//button[@id='btn_create_presentation']"));
                element.click();
                logger.pushMessage("Click \"next\"");

                for(int i = 45; i < 51; i++) {
                        element = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//input[@name='questions["+i+"]']"));
                        element.sendKeys("yes");
                }

                logger.pushMessage("Input answers to the questions");

                element = page.findElement(By.xpath("//span[contains(text(), 'Upload a video file')]"));
                element.click();

                logger.pushMessage("Click on \"upload a video file\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='dz-default dz-message']"));

                Action act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload testVideo");

                element = page.findElement(By.xpath("//button[@id='submit-form']"));
                element.click();
                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@id='main-navbar']"));

                act = new Actions(page.getPage())
                        .moveByOffset(element.getLocation().getX()+5,
                                      element.getLocation().getY()+5)
                        .click()
                        .build();
                act.perform();

                logger.pushMessage("Get warning message");
                logger.pushMessage("Close warning message");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@id='submit-form']"));
                element.click();

                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10,
                        By.xpath("//div[@id='modals-presentation-submit-success'][@aria-hidden='false']" +
                                "/div/div/div[4]/button"));
                element.click();

                logger.pushMessage("Apply changes");

                element = page.waitingForElementToBeClickable(10,
                                                By.xpath("//button[@class='close delete-presentation']"));
                element.click();
                logger.pushMessage("Delete last presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='btn btn-danger']"));
                element.click();
                logger.pushMessage("Apply delete");

                logger.closeSession();
                page.closePage();
        }

        @Test //P_HDID_008
        public void presenceOfPresentationForExpert() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("presenceOfPresentationForExpert");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click sign in button");


                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();

                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                element.sendKeys("testVideo");
                logger.pushMessage("Input title of presentation");

                element = page.findElement(By.xpath("//button[@id='btn_create_presentation']"));
                element.click();
                logger.pushMessage("Click \"next\"");

                for(int i = 45; i < 51; i++) {
                        element = page.waitingForElementToBeClickable(10,
                                        By.xpath("//input[@name='questions["+i+"]']"));
                        element.sendKeys("yes");
                }
                logger.pushMessage("Input answers to the questions");

                element = page.findElement(By.xpath("//span[@class='lbl panel-title']"));
                element.click();

                logger.pushMessage("Click on \"upload a video file\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='dz-default dz-message']"));

                Action act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload testVideo");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[contains(text(), " +
                        "'Video successfully uploaded.')]"));
                page.findElement(By.xpath("//div[@class='growl growl-notice growl-medium']/div[1]")).click();

                element = page.findElement(By.xpath("//button[@id='submit-form']"));
                element.click();

                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10,
                        By.xpath("//div[@id='modals-presentation-submit-success'][@aria-hidden='false']" +
                                "/div/div/div[4]/button"));
                element.click();

                logger.pushMessage("Apply changes");

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@class='dropdown-toggle user-menu']"));
                element.click();

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/main/logout']"));
                element.click();

                logger.pushMessage("Logout from user's account");

                signInButton = page.waitingForElementToBeClickable(10,
                                                By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.expert"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.expert"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click sign in button");
                logger.pushMessage("Login as expert");

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/reviews']"));
                element.click();

                logger.pushMessage("Open my reviews page");


                Assert.assertTrue(page.waitingForElementToBeClickable(10,
                                                        By.xpath("//*[contains(text(), 'My Reviews')]"))
                                                                .getText().trim().equalsIgnoreCase("my reviews"));

                logger.pushAssertion("Check \"My reviews\" page being opened successfully");

                element = page.findElement(By.xpath("//a[@class='dropdown-toggle user-menu']"));
                element.click();

                element = page.findElement(By.xpath("//a[@href='/main/logout']"));
                element.click();

                logger.pushMessage("Logout from Expert account");

                signInButton = page.waitingForElementToBeClickable(10,
                                                                By.xpath("//input[@class='signin-btn bg-success']"));

                element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");
                logger.pushMessage("Login as Presenter");

                element = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//button[@class='close delete-presentation']"));
                element.click();

                logger.pushMessage("Delete last presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='btn btn-danger']"));
                element.click();

                logger.pushMessage("Apply delete");

                logger.closeSession();
                page.closePage();
        }

        @Test //E_HDID_004 P_HDID_009
        public void createFeedback() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("createFeedback");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();

                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                element.sendKeys("testVideo");
                logger.pushMessage("Input title of presentation");

                element = page.findElement(By.xpath("//button[@id='btn_create_presentation']"));
                element.click();
                logger.pushMessage("Click \"next\"");

                for(int i = 45; i < 51; i++) {
                        element = page.waitingForElementToBeClickable(10,
                                                By.xpath("//input[@name='questions["+i+"]']"));
                        element.sendKeys("yes");
                }
                logger.pushMessage("Input answers to the questions");

                element = page.findElement(By.xpath("//span[@class='lbl panel-title']"));
                element.click();

                logger.pushMessage("Click on \"upload a video file\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='dz-default dz-message']"));

                Action act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload testVideo");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[contains(text(), " +
                        "'Video successfully uploaded.')]"));
                page.findElement(By.xpath("//div[@class='growl growl-notice growl-medium']/div[1]")).click();

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@id='submit-form']"));
                element.click();

                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10,
                        By.xpath("//div[@id='modals-presentation-submit-success'][@aria-hidden='false']" +
                                "/div/div/div[4]/button"));
                element.click();

                logger.pushMessage("Apply changes");

                Thread.sleep(3000);

                element = page.findElement(By.xpath("//a[@href='/reviews']"));
                element.click();
                logger.pushMessage("Open \"my rewiews\"");

                element = page.waitingForElementToBeClickable(10,
                                        By.xpath("//h2[@class='col-sm-8 text-center text-left-sm text-slim']"));

                List<WebElement> elements = page.getPage().findElements(By.xpath("//tr[@class='odd']"));

                for(WebElement tmp : elements) {
                        try {
                                element = tmp.findElement(By.xpath("//td[contains(text(), 'testVideo')]"));
                        } catch (Exception e) {
                                continue;
                        }
                        break;
                }


                element = element.findElement(By.xpath("//td/a[contains(text(), 'My Review')]"));
                element.click();

                logger.pushMessage("Open presentation \"testVideo|\"");

                WebElement playPauseBtn = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//div[@class='feedback-video start']"));
                playPauseBtn.click();

                Thread.sleep(5000);

                element = page.findElement(By.xpath("//a[contains(text(), 'Add Feedback')]"));
                element.click();

                logger.pushMessage("Add feedback to video");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='ui-slider-likedislike " +
                        "col-xs-8 ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all']/a"));

                act = new Actions(page.getPage())
                        .dragAndDropBy(element, element.getLocation().getX()+50, element.getLocation().getY())
                        .build();
                act.perform();

                element = page.findElement(By.xpath("//button[@id='submitComment']"));
                element.click();

                playPauseBtn = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//div[@class='feedback-video end']"));

                playPauseBtn.click();

                Thread.sleep(7000);

                element = page.findElement(By.xpath("//a[contains(text(), 'Add Feedback')]"));
                element.click();

                logger.pushMessage("Add another feedback to video");

                Thread.sleep(1000);

                elements = page.getPage().findElements(By.xpath("//label[@class='checkbox-inline col-md-4']/input"));
                for(WebElement tmp : elements) {
                        page.scrollToElement(tmp);
                        tmp.click();
                        Thread.sleep(50);
                }

                element = page.findElement(By.xpath("//button[@id='submitComment']"));
                element.click();

                Thread.sleep(1000);

                element = page.waitingForElementToBeClickable(10,
                                                By.xpath("//a[contains(text(), 'Submit Feedback')]"));
                element.click();

                logger.pushMessage("Submit feedback");


                element = page.waitingForElementToBeClickable(10,
                                                By.xpath("//button[contains(text(), 'Cool! I was glad to help')]"));
                element.click();

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/presentations']"));
                element.click();

                logger.pushMessage("Open My videos page");


                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='close delete-presentation']"));
                element.click();

                logger.pushMessage("Delete last presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='btn btn-danger']"));
                element.click();

                logger.pushMessage("Apply delete");

                logger.closeSession();
                page.closePage();
        }

        @Test //P_HDID_010
        public void searchPresentation() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("searchPresentation");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Add feedback to video");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");

                Thread.sleep(3000);

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();

                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                element.sendKeys("testVideo");
                logger.pushMessage("Input title of presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@id='btn_create_presentation']"));
                element.click();
                logger.pushMessage("Click \"next\"");

                for(int i = 45; i < 51; i++) {
                        element = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//input[@name='questions["+i+"]']"));
                        element.sendKeys("yes");
                }
                logger.pushMessage("Input answers to the questions");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='lbl panel-title']"));
                element.click();

                logger.pushMessage("Click on \"upload a video file\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='dz-default dz-message']"));

                Action act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload testVideo");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[contains(text(), " +
                        "'Video successfully uploaded.')]"));
                page.findElement(By.xpath("//div[@class='growl growl-notice growl-medium']/div[1]")).click();

                element = page.findElement(By.xpath("//button[@id='submit-form']"));
                element.click();

                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10,
                        By.xpath("//div[@id='modals-presentation-submit-success'][@aria-hidden='false']" +
                                "/div/div/div[4]/button"));
                element.click();

                logger.pushMessage("Apply changes");

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/reviews']"));
                element.click();

                logger.pushMessage("Open \"My reviews\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@type='search']"));
                element.click();
                page.pressButtonsFromString("testVideo");

                logger.pushMessage("Search presentation");

                Thread.sleep(1000);

                List<WebElement> elements = page.getPage()
                                        .findElements(By.xpath("//table[@id='presentations-table']/tbody/tr"));

                boolean presenceFlag = false;

                for(int i = 0; i < elements.size(); i++ ) {
                        try {
                                elements.get(i).findElement(By.xpath("//td[contains(text(), 'testVideo')]"));
                                page.scrollToElement(elements.get(i));
                        } catch (Exception e) {
                                continue;
                        }
                        presenceFlag = true;
                        break;
                }

                Assert.assertTrue(presenceFlag);
                logger.pushAssertion("Check if presentation was found");

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/presentations']"));
                element.click();

                logger.pushMessage("Open \"my videos\" page");

                element = page.waitingForElementToBeClickable(10,
                                                By.xpath("//button[@class='close delete-presentation']"));
                element.click();

                logger.pushMessage("Delete last presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='btn btn-danger']"));
                element.click();

                logger.pushMessage("Apply delete");

                logger.closeSession();
                page.closePage();
        }

        @Test //P_HDID_011
        public void rejectFeedback() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("rejectFeedback");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();

                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                element.sendKeys("testVideo");
                logger.pushMessage("Input title of presentation");

                element = page.findElement(By.xpath("//button[@id='btn_create_presentation']"));
                element.click();
                logger.pushMessage("Click \"next\"");

                for(int i = 45; i < 51; i++) {
                        element = page.waitingForElementToBeClickable(10, By.xpath("//input[@name='questions["+i+"]']"));
                        element.sendKeys("yes");
                }
                logger.pushMessage("Input answers on the questions");

                element = page.findElement(By.xpath("//span[@class='lbl panel-title']"));
                element.click();

                logger.pushMessage("Click on \"upload a video file\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[@class='dz-default dz-message']"));

                Action act = new Actions(page.getPage())
                        .moveToElement(element)
                        .click()
                        .build();
                act.perform();

                Thread.sleep(3000);

                page.pressButtonsFromString(property.getProperty("path.testVideo"));
                page.pressButton(KeyEvent.VK_ENTER);

                logger.pushMessage("Upload testVideo");

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[contains(text(), " +
                        "'Video successfully uploaded.')]"));
                page.findElement(By.xpath("//div[@class='growl growl-notice growl-medium']/div[1]")).click();

                element = page.findElement(By.xpath("//button[@id='submit-form']"));
                element.click();

                logger.pushMessage("Submit changes");

                element = page.waitingForElementToBeClickable(10,
                        By.xpath("//div[@id='modals-presentation-submit-success'][@aria-hidden='false']" +
                                "/div/div/div[4]/button"));
                element.click();

                logger.pushMessage("Apply changes");

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@class='dropdown-toggle user-menu']"));
                element.click();

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/main/logout']"));
                element.click();

                logger.pushMessage("Logout from user account");

                signInButton = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.expert"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.expert"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");
                logger.pushMessage("Login as Expert");

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/reviews']"));
                element.click();

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[contains(text(), 'Reject')]"));
                element.click();

                Thread.sleep(1000);

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@class='dropdown-toggle user-menu']"));
                element.click();

                element = page.waitingForElementToBeClickable(10, By.xpath("//a[@href='/main/logout']"));
                element.click();

                logger.pushMessage("Logout from Expert account");

                signInButton = page.waitingForElementToBeClickable(10,
                                                        By.xpath("//input[@class='signin-btn bg-success']"));

                element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");
                logger.pushMessage("Login as Presenter");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='close delete-presentation']"));
                element.click();

                logger.pushMessage("Delete last presentation");

                element = page.waitingForElementToBeClickable(10, By.xpath("//button[@class='btn btn-danger']"));
                element.click();

                logger.pushMessage("Apply delete");

                logger.closeSession();
                page.closePage();

        }

        @Test //P_HDID_012
        public void creationPresentationWithoutTitle() throws Exception {
                WebElement signInButton;
                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("creationPresentationWithoutTitle");
                logger.pushMessage("Open "+page.getPage().getTitle());

                signInButton = page.findElement(By.xpath("//input[@class='signin-btn bg-success']"));

                Assert.assertTrue(signInButton.getAttribute("value").trim().equalsIgnoreCase("SIGN IN"));
                logger.pushAssertion("Check Login page presence");

                WebElement element = page.findElement(By.xpath("//input[@id='email_id']"));
                element.sendKeys(property.getProperty("login.presenter"));
                logger.pushMessage("Input valid user's login");
                element = page.findElement(By.xpath("//input[@id='password_id']"));
                element.sendKeys(property.getProperty("pass.presenter"));
                logger.pushMessage("Input valid user's password");

                signInButton.click();
                logger.pushMessage("Click Sign in button");

                element = page.waitingForElementToBeClickable(10, By.xpath("//span[@class='add-btn-description']"));
                element.click();

                logger.pushMessage("Click \"create new video\"");

                element = page.waitingForElementToBeClickable(10, By.xpath("//input[@id='name_create_presentation']"));
                logger.pushMessage("Write empty string into the title of presentation");

                element = page.findElement(By.xpath("//button[@id='btn_create_presentation']"));
                element.click();

                logger.pushAssertion("Check Error message is appearing");
                Assert.assertTrue(page.findElement(
                                        By.xpath("//div[@class='jquery-validate-error help-block']")).isDisplayed());

                logger.closeSession();
                page.closePage();
        }

        @Test //P_HDID_013
        public void logOutTest() throws Exception {
                logger.openSession("logoutTest");
                logger.pushMessage("All actions were realized in previous tests");
                logger.closeSession();
        }

        @Test //P_HDID_014
        public void forgotPassword() throws Exception {

                page.openPage("http://app.howdidido.co/en/main/login", DriverType.CHROME);
                logger.openSession("forgotPassword");
                logger.pushMessage("Open "+page.getPage().getTitle());

                WebElement element = page.findElement(By.xpath("//a[@id='forgot-password-link']"));
                element.click();

                logger.pushMessage("Click \"forgot password\"");

                element = page.findElement(By.xpath("//input[@class='signin-btn bg-primary']"));
                Assert.assertTrue(element.isDisplayed());

                logger.pushAssertion("Check \"send password reset link\" button is displayed");

                logger.closeSession();
                page.closePage();
        }

        @AfterClass
        public static void tearDown() throws InterruptedException {
                logger.finalize();
        }

}
