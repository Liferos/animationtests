package busfor.ua;

;
import browser.WebDriverFactory;
import org.junit.*;
import org.openqa.selenium.*;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

//test need to refactoring
public class FirefoxTest {

        public static final String mainUrl = "http://www.busfor.ua/";
        public static WebDriver webDriver = null;
        public static WebDriverFactory wdFactory = new WebDriverFactory();;

        @BeforeClass
        public static void setUp() {
                //wdFactory.setWebDriver(DriverType.FIREFOX);
                //webDriver = wdFactory.openPage(mainUrl);
                //webDriver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
                //webDriver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
        }

        @Ignore
        @Test
        public void openPage() {
                Assert.assertTrue(webDriver.getTitle()
                        .equals("Купить билеты на автобус онлайн, заказать автобусные билеты | Busfor Украина"));
        }

        @Ignore
        @Test
        public void insertData() throws Exception {
                List<WebElement> elements = null;
                WebElement element = null;
                element = webDriver.findElement(By.xpath("html/body/div[4]/div[1]/div[1]/" +
                        "div[3]/div/div/div/form/div[1]/input[1]"));
                element.sendKeys("Киев");
                element.click();
                webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
                element = webDriver.findElement(By.xpath("//div[@class='autocomplete-suggestion " +
                        "autocomplete-selected']"));
                element.click();
                element = webDriver.findElement(By.xpath("html/body/div[4]/div[1]/div[1]/" +
                        "div[3]/div/div/div/form/div[2]/input[1]"));
                element.click();
                element.sendKeys("Винница");
                webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                element = webDriver.findElement(By.xpath("html/body/div[8]/div[1]"));
                element.click();
                webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                element = webDriver.findElement(By.xpath("//input[@class='form-control js-datepicker-departure']"));
                element.click();
                webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

                element = webDriver.findElement(By.xpath("//div[@class='datepicker-days']"));
                elements = element.findElements(By.xpath("//td[@class='day']"));

                for(WebElement webElement: elements) {
                        if(webElement.getText().trim().equals("28")) {
                                webElement.click();
                                break;
                        }
                }

                webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                webDriver.findElement(By.xpath(".//*[@id='new_search']/div[5]/input")).click();


                webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                elements = webDriver.findElements(By.xpath(".//div[@class='variant clearfix fadeIn']"));
                elements.addAll(webDriver.findElements(By.xpath(".//div[@class='variant clearfix']")));

                element = elements.get(elements.size()-1)
                        .findElement(By.xpath("//button[@class='btn btn-primary pull-right']"));
                element.click();

                webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                element = webDriver.findElement(By.xpath(".//input[@class='btn btn-primary pull-right']"));
                System.out.println(element.isDisplayed()+" "+element.getText());
                element.click();

                //данные пассажиров
                element = webDriver.findElement(By.xpath("//input[@id='order_passengers_attributes_0_last_name']"));
                element.sendKeys("lastName");

                element = webDriver.findElement(By.xpath("//input[@id='order_passengers_attributes_0_first_name']"));
                element.sendKeys("firstName");

                element = webDriver.findElement(By.xpath("//input[@id='order_phone']"));
                element.click();
                element.sendKeys("633056211");

                element = webDriver.findElement(By.xpath("//input[@id='order_email']"));
                element.sendKeys("RickLestatDT@gmail.com");

                element = webDriver.findElement(By.xpath("//input[@id='agreementCheckbox']"));
                element.click();

                element = webDriver.findElement(By.xpath("html/body/div[4]/div[1]/div[1]/div/div[2]/form/div[5]/input"));
                element.click();

                Assert.assertTrue("ВЫБЕРИТЕ СПОСОБ ОПЛАТЫ".equalsIgnoreCase(
                        webDriver.findElement(By.xpath("//div[@class='col-sm-8 col-xs-12 pull-left']/h2"))
                                .getText().trim()));

        }

        @Ignore
        @Test
        public void scrollRadioButtonTest() throws Exception {
                List<WebElement> elements = null;
                WebElement element = null;

                elements = webDriver.findElements(By.xpath("//div[@class='routes_radio']"));

                element = elements.get(new Random().nextInt(elements.size()-1));


                ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true)", element);

                webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                element.click();

                elements = webDriver.findElements(By.xpath("//input[@class='form-control js-datepicker-departure']"));
                elements.get(1).click();

                element = webDriver.findElement(By.xpath("//div[@class='datepicker-days']"));
                elements = element.findElements(By.xpath("//td[@class='day']"));

                elements.get(new Random().nextInt(elements.size()-1)).click();

                element = webDriver.findElement(By.xpath(".//div[@class='routes_submit']"));
                element.click();

                webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

                element = webDriver.findElement(By.xpath("html/body/div[4]/div[1]" +
                        "/div[2]/div[1]/div[2]/div/div/p/span"));

                Assert.assertTrue(element
                        .getText().trim().equalsIgnoreCase("Рейсы не найдены"));

        }



}
