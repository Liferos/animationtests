package PowtoonVisRedactor;

import browser.DriverType;
import org.im4java.core.CompareCmd;
import org.im4java.core.IMOperation;
import org.im4java.process.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import page.Page;

import java.io.*;
import java.util.Calendar;

public class AnimationTest {

        private static Page page;

        @BeforeClass
        public static void setUp() {
                //page = new Page().openPage("http://192.168.2.72/powtoon/GSAP%20with%20PowToon%20Animation/Index.html",
                page = new Page().openPage("http://192.168.2.72/powtoon/GSAP%20with%20PowToon%20Animation/Index.html",
                                           DriverType.CHROME);
                page.getPage().manage().window().maximize();
                page.waiting(20);

        }

        @Test
        public void compareFrames() throws Exception {
                //clear AnimationFrames folder
                File animFrames = new File("AnimationFrames/");
                for(File f : animFrames.listFiles()) {
                        f.delete();
                }
                WebElement rElement;
                WebElement lElement;
                CompareCmd compare = new CompareCmd();
                IMOperation imo = new IMOperation();
                Actions builder = new Actions(page.getPage());


                compare.setOutputConsumer(StandardStream.STDOUT);
                ProcessStarter.setGlobalSearchPath("E:\\Program Files\\Java\\ImageMagick-6.9.1-Q16"); //path to image
                                                                                                      //magick.
                                                                        //need to use 6.9.1-Q16 version

                Thread.sleep(5000);



                imo.metric("ae");       //set metric
                //imo.fuzz(65350.0);    //fuzz-factor - num of ignored pixels
                //imo.fuzz(40000.0);

                imo.addImage();         //set used files
                imo.addImage();
                imo.addImage();


                WebElement nextFrame = page.findElement(By.xpath("//button[@id='next']"));
                nextFrame.isDisplayed();


                Action act = builder
                                .click(nextFrame)
                                .build();

                File log = new File("log.txt");  //recreate log file
                log.delete();
                log.createNewFile();

                PrintStream fw = new PrintStream(log); //writer for log file
                fw.print("Animation comparison log. ");
                fw.println(Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
                         +":"+
                         Calendar.getInstance().get(Calendar.MINUTE));
                fw.println(" ");
                fw.flush();

                for(int i = 0;i < 114; i++) {
                        lElement = page.waitingForElementToBeLocated(10, By.xpath("//div[@class='rightBox']/div[2]"));
                        rElement = page.waitingForElementToBeLocated(10, By.xpath("//div[@class='leftBox']/div[2]"));
                        //find needed elements and wait 1s
                        Thread.sleep(1000);

                        OutputStream os = new FileOutputStream("AnimationFrames/"+i+"_img1.jpg"); //load first img
                        os.write(page.getObjectScreenshotAsByte(lElement));
                        os.close();

                        os = new FileOutputStream("AnimationFrames/"+i+"_img2.jpg"); //load second img
                        os.write(page.getObjectScreenshotAsByte(rElement));
                        os.close();

                        act.perform();

                        File file = new File("AnimationFrames/"+i+"_difference.jpg"); //create difference file
                        file.createNewFile();

                        try {
                                compare.run(imo,
                                            "AnimationFrames/"+i+"_img1.jpg",
                                            "AnimationFrames/"+i+"_img2.jpg",
                                            "AnimationFrames/"+i+"_difference.jpg");//adding path for files,
                                                                                    // look 52-54 str
                                System.out.println("Frames #"+i+" haven't difference!");
                        } catch (Exception ex) {
                                int difference = Integer.parseInt(ex.getMessage().substring(
                                                                        ex.getMessage().indexOf(":")+1,
                                                                        ex.getMessage().length())
                                                                  .trim());
                                fw.print((i+1)+"-frame.  Difference: "+difference);
                                fw.println(" "+difference/2000+"%");
                                fw.flush();
                                System.out.println(ex.getMessage());
                                System.out.println("Frames #"+i+" have difference!");
                                continue;
                                //if throw "CommandException" - img have difference
                                //looking for details in the log file
                        }
                }
                fw.close();


        }


        @AfterClass
        public static void tearDown() throws InterruptedException {
                page.waiting(5);
                page.closePage();
        }

}
