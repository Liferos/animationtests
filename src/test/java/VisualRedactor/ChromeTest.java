package VisualRedactor;

import browser.DriverType;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import page.Page;

import java.util.List;
import java.util.Random;

public class ChromeTest {

        public static Page page;

        @BeforeClass
        public static void setUp() {
                page = new Page().openPage("http://192.168.2.72/Frontend/#", DriverType.CHROME);
                page.getPage().manage().window().maximize();
        }

        @Ignore
        @Test
        public void testBasicFunctional() {

        }

        @Ignore
        @Test
        public void testBackground() {

        }

        @Ignore
        @Test
        public void tempTest() {
        }

        @Test
        public void testChangeOpacityOfAddedObject() throws Exception {
                WebElement element = null;
                element = page.findElement(By.xpath("//a[@id='m_Library']"));
                element.click();

                List<WebElement> elements = null;

                element = page.waitingForElementToBeClickable(10, By.xpath("//img[@src='assets/library/asset3.png']"));

                WebElement target = page.findElement(By.xpath("//div[@id='powtoon_container']"));

                Actions builder = new Actions(page.webDriver);

                Action act = builder
                        .dragAndDrop(element, target)
                        .build();
                act.perform();


                element = page.waitingForElementToBeLocated(5, By.xpath(".//div[@id='elem0']"));
                Assert.assertTrue(element.isDisplayed());
                element.click();

                page.waiting(5);
                element = page.findElement(By.xpath("//i[contains(text(),'transform')] "));
                act = builder
                        .moveToElement(element)
                        .clickAndHold()
                        .build();
                act.perform();

                page.waiting(5);

                element = page.findElement(By.xpath("//input[@id='opacitySlider']"));
                act = builder
                        .dragAndDropBy(element,
                                       element.getLocation().getX()-50,
                                       element.getLocation().getY())
                        .build();
                act.perform();

                page.waiting(10);

                element = page.findElement(By.xpath("//*[name()='svg'][@xmlns='http://www.w3.org/2000/svg']"));

                Assert.assertTrue(Double.parseDouble(element.getCssValue("opacity")) == 0.5);

        }

        @Test
        public void testChangeColorOfAddedText() {
                WebElement element = null;
                List<WebElement> elements = null;

                element = page.findElement(By.xpath("//a[@id='m_Texts']"));
                element.click();

                element = page.waitingForElementToBeClickable(10, By.xpath("//div[text()='Large text']"));

                WebElement target = page.findElement(By.xpath("//div[@id='powtoon_container']"));

                Actions builder = new Actions(page.webDriver);

                Action act = builder
                        .dragAndDrop(element, target)
                        .build();
                act.perform();


                element = page.waitingForElementToBeLocated(5, By.xpath(".//div[@id='elem0']"));
                element.click();

                page.waiting(5);
                element = page.findElement(By.xpath("//*[text()='palette']"));
                element.click();

                page.waiting(5);

                element = page.findElement(By.xpath("//div[@class='colorpicker_color']"));


                int randx = new Random().nextInt(element.getSize().getWidth()-1);
                int randy = new Random().nextInt(element.getSize().getHeight()-1);

                element = page.findElement(By.xpath("//div[@class='colorpicker_color']/div/div"));

                act = builder
                        .dragAndDropBy(element, randx, randy)
                        .build();
                act.perform();

                element = page.findElement(By.xpath("//div[@class='colorpicker_hue']"));

                randy = new Random().nextInt(element.getSize().getHeight()-1);

                act = builder
                        .dragAndDropBy(element, element.getLocation().getX(), randy)
                        .build();
                act.perform();

                int r = new Random().nextInt(254)+1;
                int g = new Random().nextInt(254)+1;
                int b = new Random().nextInt(254)+1;

                System.out.println(r+" "+b+" "+g);
                page.waiting(5);

                element = page.findElement(By.xpath("//div[@class='colorpicker_rgb_r colorpicker_field']/input"));
                act = builder
                        .click(element)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(String.valueOf(r))
                        .build();
                act.perform();
                page.waiting(5);

                element = page.findElement(By.xpath("//div[@class='colorpicker_rgb_g colorpicker_field']/input"));
                act = builder
                        .click(element)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(String.valueOf(g))
                        .build();
                act.perform();
                page.waiting(5);

                element = page.findElement(By.xpath("//div[@class='colorpicker_rgb_b colorpicker_field']/input"));
                act = builder
                        .click(element)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.ARROW_RIGHT)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(Keys.BACK_SPACE)
                        .sendKeys(String.valueOf(b))
                        .build();
                act.perform();

                page.waiting(5);

                element = page.findElement(By.xpath("//div[@id='elem0']"));

                String cssColor = element.getCssValue("color").replace("rgba(", "").replace(")", "");
                System.out.println(cssColor);

                String[] colors = cssColor.split(",");

                //сравнить данные полученные из атрибутов с переменными r,g,b соответственно

                page.waiting(10);

        }

        @AfterClass
        public static void tearDown() throws Exception {
                Thread.sleep(10000);
                page.closePage();
        }

}
